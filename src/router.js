import Vue from 'vue';
import Router from 'vue-router';
import MainPage from '@/pages/MainPage';
import ErrorPage from '@/pages/ErrorPage';
import PostPage from '@/pages/PostPage';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'main_page',
      component: MainPage,
    },
    {
      path: '/error',
      name: 'error_page',
      component: ErrorPage,
    },
    {
      path: '/post',
      name: 'post_page',
      component: PostPage,
    },
  ],
});

export default router;
