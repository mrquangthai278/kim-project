export const listError = [
  'Wolf— phiên âm chữ này /wʊlf/, khi đọc đừng để sót âm /f/ cuối nhé.',
  'Starving— thiếu âm /r/ cong lưỡi ở giữa nha.',
  'House— thiếu /s/ cuối em nhé, phiên âm chữ này là /haʊs/',
  'Don’t— mình không đọc là /dong/ mà âm /d/ của mình có bật âm /t/ nữa nhé.',
  'Continue— phiên âm chữ này là /kənˈtɪnjuː/, mình đọc tách ghép âm sẽ là / kən-ˈtɪ-njuː/, chú ý âm /u:/ cuối phải u miệng lại và kéo dài ra nha.',
  'Well—âm /l/ cuối khi đọc mình chấm đầu lưỡi ra đằng sau mặt răng cửa nha, mình đọc là /ồ/ nhé. Phiên âm chữ này là /we-l/',
  'Regularly— chú ý những âm này nếu như chưa biết cách đọc, em nên tra từ điển để có thể đọc rõ từng âm chứ đừng đọc nuốt sẽ hư âm hết em nhé, phiên âm chữ này là /ˈre-ɡjə-lə-li/ nha.',
  'If—cuối âm mình có /f/, em đọc chậm để mình tạo khẩu hình /f/ trước khi kết thúc âm nha.',
  'Think— phiên âm chữ này là /θɪŋk/, chú ý âm /θ/ này mình để khẩu hình trước, lưỡi đưa ra khỏi hai hàm răng cắn nhẹ lại, sau đó khi mình đọc thì âm sẽ tự giật vào trong miệng em nha.',
];
