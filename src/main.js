import Vue from 'vue';
import 'babel-polyfill';
import '@/plugins/vuetify';
import interceptorsSetup from './utils/interceptors';
import localStorageSetup from './utils/version';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;
Vue.prototype.$vueEventBus = new Vue();

interceptorsSetup();
localStorageSetup();

Vue.mixin({
  methods: {
    goToRoute: (routeName, routeParams) => {
      return router.push({
        name: routeName,
        params: routeParams,
      });
    },
    alert({ type, message }) {
      store.commit('updateModalData', { type, message });
      setTimeout(() => {
        if (store.getters.getModalData.show) {
          return store.commit('turnOffModal');
        }
      }, 2000);
    },
  },
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
