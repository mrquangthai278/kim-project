import store from '@/store';

export default function setup() {
  const app_version = process.env.VUE_APP_VERSION;
  if (store.getters.appVersion !== app_version) {
    // store.commit('logout');
    store.commit('updateAppVersion', app_version);
  }
}
