export default {
  updateAppVersion(state, version) {
    state.version = version;
  },
  initAccessTokenFb(state, token) {
    state.access_token_fb = token;
  },
  updateUsernameFb(state, name) {
    state.user_name_fb = name;
  },
  updateBackColor(state, color) {
    state.back_color = color;
  },
  updateModalData(state, { type, message }) {
    state.modal_data.show = true;
    state.modal_data.message = message;
    state.modal_data.type = type;
  },
  turnOffModal(state) {
    state.modal_data.show = false;
    state.modal_data.message = '';
  },
};
