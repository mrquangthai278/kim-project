import api from '@/plugins/axios';

export default {
  async exportError({}, formData) {
    try {
      const respone = await api.post('/link-export', formData);
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  // API LINK
  async createLink({}, formData) {
    try {
      const respone = await api.post('/link', formData);
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  async getLinkVideo({}, link) {
    try {
      const respone = await api.get('/link', {
        params: {
          link,
        },
      });
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  async updateLink({}, formData) {
    try {
      const respone = await api.put('/link', formData);
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  // API POST
  async getListPost({}, searchText) {
    try {
      const respone = await api.get('/post', {
        params: {
          name: searchText,
        },
      });
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  async createPost({}, formData) {
    try {
      const respone = await api.post('/post', formData);
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  async deletePost({}, id) {
    try {
      const respone = await api.delete('/post', {
        params: {
          id,
        },
      });
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  async updatePost({}, formData) {
    try {
      const respone = await api.put('/post', formData);
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  async getPostError({}, { post_id }) {
    try {
      let respone = await api.get(`/post/${post_id}/error`);
      respone.data.map(item => {
        if (item.word) item.full_text = `${item.word} - ${item.description}`;
        else item.full_text = `${item.description}`;
      });
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  // API Error
  async getListError({}, searchText) {
    try {
      const respone = await api.get('/error', {
        params: {
          word: searchText,
        },
      });
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  async createError({}, formData) {
    try {
      const respone = await api.post('/error', formData);
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  async createErrorNote({}, formData) {
    try {
      const respone = await api.post('/error/note', formData);
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  async deleteError({}, id) {
    try {
      const respone = await api.delete('/error', {
        params: {
          id,
        },
      });
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  async updateError({}, formData) {
    try {
      const respone = await api.put('/error', formData);
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  // API All
  async listAllError({}) {
    try {
      let respone = await api.get('/error/all');
      respone.data.map(item => {
        if (item.word) item.full_text = `${item.word} - ${item.description}`;
        else item.full_text = `${item.description}`;
      });
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
  async listAllPost({}) {
    try {
      const respone = await api.get('/post/all');
      return { ok: true, data: respone.data };
    } catch (error) {
      return { ok: false, data: error };
    }
  },
};
