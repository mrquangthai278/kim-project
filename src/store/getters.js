export default {
  appVersion(state) {
    return state.version;
  },
  getTokenFb(state) {
    return state.access_token_fb;
  },
  getUserNameFb(state) {
    return state.user_name_fb;
  },
  getBackColor(state) {
    return state.back_color;
  },
  getModalData(state) {
    return state.modal_data;
  },
};
