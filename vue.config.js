const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data:
          '\n @import "@/assets/styles/Variables/bulma.scss";\n',
      },
    },
  },
  productionSourceMap: false,
  baseUrl: process.env.VUE_APP_ASSET_BASE_URL,
  outputDir: 'dist',
  assetsDir: 'src/assets',
  runtimeCompiler: false,
  parallel: true,
  chainWebpack: config => {
    config.plugins.delete('preload');
    config.plugins.delete('prefetch');
  },
  lintOnSave: true,
  configureWebpack: {
    plugins: [
      new CompressionPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: new RegExp('\\.(js|css)$'),
        threshold: 10240,
        minRatio: 0.8,
      }),
    ],
    output: {
      filename: '[name]-[hash].js',
    },
  },
};
