# How styling work in Meta-SPA

We're using *scss* as preprocessor for this project

## Vue basic rules
- Use scoped whenever you want your clases to be unique (Not conflict with other classes from other components)

## Global imports
- We should (must) only put variables/mixin/function instead of to-be-compiled styles within global imports

Global imports for styles will be configured in vue.config.js
```javascript
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/assets/styles/variables/theme.scss";
          @import "@/assets/styles/variables/bulma.scss";
          @import "@/assets/styles/variables/layout.scss";
        `,
      },
    },
  },
  // All these are variables and not process to be added into "style" blocks
```
